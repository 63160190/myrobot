/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author User
 */
public class TableMap {
    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap(){
        showtitle();
        for(int y=0;y<height;y++){
            for(int x=0; x<width;x++){
                if(robot.isOn(x,y)){
                    showrobot();
                }else if (bomb.isOn(x,y)){                
                    showbomb();
                }else{
                    showcell();
                }
        }
            showline();
    }
    }     

    private void showtitle() {
        System.out.println("Map");
    }

    private void showline() {
        System.out.println("");
    }

    private void showcell() {
        System.out.print("-");
    }

    private void showbomb() {
        System.out.print(bomb.getSymbo());
    }

    private void showrobot() {
        System.out.print(robot.getSymbol());
    }
    public boolean inMap(int x,int y){
        //x -> 0-(width-1),y -> 0-(height-1)
        return (x>=0 && x<width)&&(y>=0 && y< height);
    }
    public boolean isBomb(int x,int y){
        return bomb.getX()==x&&bomb.getY()==y;
    }
}
